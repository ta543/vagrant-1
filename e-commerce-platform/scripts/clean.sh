#!/bin/bash

echo "Cleaning up the environment..."

# Stop all containers
docker-compose -f /path/to/docker-compose.yml down

# Remove unused Docker images
docker image prune -a -f

echo "Clean-up completed successfully."
