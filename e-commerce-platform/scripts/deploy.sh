#!/bin/bash

echo "Deploying application..."

# Pull the latest containers
docker-compose -f /path/to/docker-compose.yml pull

# Restart services
docker-compose -f /path/to/docker-compose.yml up -d --remove-orphans

echo "Deployment completed successfully."
