#!/bin/bash

echo "Setting up the environment..."

# Install Docker
echo "Installing Docker..."
sudo apt-get update && sudo apt-get install -y docker.io

# Install Docker Compose
echo "Installing Docker Compose..."
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Start Docker services
echo "Starting Docker services..."
docker-compose -f /path/to/docker-compose.yml up -d

echo "Setup completed successfully."
