#!/bin/bash

echo "Running tests..."

# Run unit tests
echo "Running unit tests..."
docker exec -it myapp-backend npm run test

# Integration tests can be added here
echo "Running integration tests..."
# docker exec -it myapp-backend /path/to/integration-test.sh

echo "Testing completed successfully."
