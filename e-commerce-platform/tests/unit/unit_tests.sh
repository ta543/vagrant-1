#!/bin/bash
echo "Running Unit Tests..."
# Navigate to the backend service directory
cd path/to/backend/service
# Run Jest tests or any other test runner you have configured
npm test
echo "Unit tests completed."
