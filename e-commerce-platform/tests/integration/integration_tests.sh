#!/bin/bash
echo "Running Integration Tests..."
# Example: Testing user registration and login flow
echo "Testing user registration..."
curl -s -o /dev/null -w "%{http_code}" -X POST http://localhost:3000/api/register -d '{"username":"testuser", "password":"testpass"}' -H "Content-Type: application/json"
if [ "$?" -eq 200 ]; then
    echo "User registration test passed."
else
    echo "User registration test failed."
fi
echo "Testing user login..."
curl -s -o /dev/null -w "%{http_code}" -X POST http://localhost:3000/api/login -d '{"username":"testuser", "password":"testpass"}' -H "Content-Type: application/json"
if [ "$?" -eq 200 ]; then
    echo "User login test passed."
else
    echo "User login test failed."
fi
echo "Integration tests completed."
