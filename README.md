# 🛒 E-Commerce Platform

Welcome to the repository for our E-Commerce Platform, showcasing a modern microservices architecture using Vagrant, Docker, Ansible, and integrated monitoring tools via GitLab CI/CD 🚀.

## 📋 Project Overview

This e-commerce platform simulates a simple online store which includes services for:
- **Frontend:** A web application built with React and served via Nginx 🌐.
- **Backend:** A Node.js application providing REST APIs 🚧.
- **Authentication:** A Python Flask application managing user authentication 🔐.
- **Database:** A PostgreSQL database 📦.

All components are containerized with Docker and orchestrated with Docker Compose.

### 🏗 Architecture

The project is structured as follows:
- **Frontend:** A React application served via Nginx.
- **Backend:** A Node.js application providing REST APIs.
- **Authentication:** A Python Flask application managing user authentication.
- **Database:** A PostgreSQL database.

## 🔧 Prerequisites

To run and develop the e-commerce platform locally, you will need:
- Vagrant 🏕
- Docker & Docker Compose 🐳
- Ansible (for configuration management) ⚙️
- Node.js and npm (for backend development) 📦
- Python 3 and pip (for authentication service) 🐍

## 🛠 Local Development Setup

1. **Clone the repository:**
   ```bash
   git clone https://example.com/e-commerce-platform.git
   cd e-commerce-platform
   ```

### Start the virtual machines via Vagrant:
```bash
vagrant up
```

### Install dependencies and start services:
Navigate to each service directory and follow the README instructions to start each component.

## 🧪 Testing

### Running Tests
To execute all unit and integration tests:

```bash
cd tests
./unit/unit_tests.sh
./integration/integration_tests.sh
```

## 🚀 Deployment

### Building and Deploying with GitLab CI/CD
This project uses GitLab CI/CD for continuous integration and deployment. The pipeline includes stages for testing, building Docker images, and deploying to production.

Refer to .gitlab-ci.yml for pipeline configurations.

## 📊 Monitoring

### ELK Stack
Elasticsearch for storing logs.
Logstash for processing and forwarding logs to Elasticsearch.
Kibana for visualizing logs.

### Prometheus & Grafana
Prometheus for monitoring metrics.
Grafana for visualizing the metrics collected by Prometheus.

Access the monitoring dashboard via:

Kibana: http://localhost:5601
Grafana: http://localhost:3000

## 📄 License
This project is licensed under the MIT License - see the LICENSE.md file for details.

